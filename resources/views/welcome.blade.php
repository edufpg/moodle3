<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: rgb(141, 231, 5);
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }
            .img{
                border: 0.5mm solid rgb(141, 231, 5);

            }
            .links > a {
                color: rgb(141, 231, 5);
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;

                border: 0.5mm solid ;
            }
            a:hover{
                color:white;
                background-color: rgb(141, 231, 5);
            }
            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>

        <div class="flex-center position-ref full-height">


            <div class="content">
                <div class="title m-b-md" >
                    <img class="img" src="{{URL::asset('images/logo.png')}}" alt="">
                </div>

                <div class="links">
                    @if (Route::has('login'))

                        @auth

                            <a href="{{ url('/alumnos') }}">Alumno</a>
                        @else
                            <a href="{{ route('login') }}">Acceso</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}">Registro</a>
                            @endif
                        @endauth

                @endif
                </div>
            </div>
        </div>
    </body>
</html>
