<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignatura extends Model
{
    public function profesor(){
        return $this->belongsTo('App\Profesor');
    }
    public function asignatura(){
        return $this->belongsTo('App\Asignatura');
    }
}
