<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    public function asignaturas(){

        return $this->hasMany('App\Asignatura','llave2','id');
    }
    public function alumnos(){
        return $this->hasMany('App\Alumno','llave','id');
    }
}
