<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $fillable = [
        'nombre', 'email', 'DNI','disposicion',
    ];
    public function alumno(){
        return $this->belongsTo('App\Profesor');
    }
}
