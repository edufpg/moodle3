<?php

namespace App\Http\Controllers;

use App\Alumno;
use Illuminate\Http\Request;
use App\Profesor;
use App\Tarea;
use App\Nota;


class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        $alumno = Alumno::where('email',auth()->user()->email)->get()->first();
        if(empty($alumno[0])){


            return view('auth.alumno.none');

        }else{

            return view('auth.alumno.home');
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $profesores = Profesor::all();
        $tareas = Tarea::all();
        $notas = Nota::all();

        return view();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alumno = new Alumno();
        $alumno->DNI = $request->DNI;
        $alumno->nombre = $request->nombre;
        $alumno->apellidos = $request->apellidos;
        $alumno->nacimiento = $request->nacimiento;
        $alumno->asignaturas = $request->asignaturas;
        $alumno->edad = $request->edad;
        $alumno->profesors_id = $request->profesors_id;
        $alumno->save();

        return route('/home');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function show(Alumno $alumno)
    {
        $alumno = Alumno::find($alumno->id);
        return view('alumnoshow',['alumno'=>$alumno]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function edit(Alumno $alumno)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Alumno $alumno)
    {

        $alumno->DNI = $request->DNI;
        $alumno->nombre = $request->nombre;
        $alumno->apellidos = $request->apellidos;

        $alumno->asignaturas = $request->asignaturas;
        return route('/alumnos');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alumno $alumno)
    {
        Alumno::destroy($alumno);
        return route('/');
    }
}
