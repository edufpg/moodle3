<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('disposicion');
            $table->text('email');
            $table->text('nombre');
            $table->text('apellidos');
            $table->char('DNI',10);
            $table->text('nacimiento');
            $table->text('asignaturas');
            $table->integer('edad');
            $table->timestamps();
            $table->integer('profesors_id')->unsigned();

            $table->foreign('profesors_id')->references('id')->on('profesors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
